--------------------------------------------------------------------------
Package:      tipauni
Author:       Niranjan
Version:      v0.1  (27 April, 2021)
Description:  For producing Unicode characters with TIPA commands.
Repository:   https://gitlab.com/niruvt/tipauni
Bug tracker:  https://gitlab.com/niruvt/tipauni/-/issues
License:      GPL v3.0+
--------------------------------------------------------------------------
